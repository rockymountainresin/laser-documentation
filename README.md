# Fort Collins Creator Hub Laser Guide

This is designed to be a resource for those interested in etching & cutting on the hub's CamFive 2416 and the K40 lasers. This is a supplement to the *intro to laser cutting class*, that is **required** before operating these machines.

## Overview

This is our best attempt at centralizing our knowledge of laser cutting and etching. We hope to share some insight and lessons learned from projects made at the hub. There are many ways to use the laser cutter, and we hope to allow you to discover your own uses for it. 🎅

## Getting Started

**Note:** Safety is a primary concern when operating this kind of machinery. The laser is *literally* burning through the material, and can become a fire hazard. The best prevention of fires is by using proper air pressure and flow, only cutting approved materials, and proactively monitoring the machine. The air coming out of the head of the laser *should* put out any fires before they start, but it is important to how to react to a fire.

- **DO** **NOT** leave the laser unattended while running.
- **DO** **NOT** cut anything on the list of banned materials.

## Operating instructions

There are 4 key systems to be aware of when using the laser cutters at FCCH.

- The air compressor and regulator
- The exhaust fan and vent
- The laser cutting machine
- The laser cutting software

### Air compressor

- [ ] is `On` and air is flowing.
  - The compressor is located in the woodshop, and will need to be turned `On`, and have it's shut off valve `Opened`. *This is the red leaver attached to the output line of the compressor, turn it so that it's parallel with the line.*
- [ ]  The machine's air regulator `Open`, and air is flowing at 15-20 psi.
  - The Regulator is the *blue* leaver located next to the machine. You *should* hear air coming out of the laser cutting nozzle.

### Exhaust

- [ ] The knob for the Exhaust fan is set to `High`.
  - This is located on the wall next to the *Claymore* and *K40* lasers. *Spin the knob just slightly clockwise to turn it on and at high.*
- [ ] The laser's exhaust baffle is `Open`.
  - This is located next to the machine. *Slide the baffle open*
  
### Laser cutter

- [ ] Machine is `On`,
  - Flip the big switch on machine. *It's the *blue* breaker on the right side of the *CamFive*, and the black surge protector for the *K40*.*
- [ ] The laser head is focused on the material at the proper hight.
  - 60mm to the top of the white piece of plastic on the *K40*, 5mm to the bottom of the laser nozzle on the *CamFive*.
- [ ] Laser is `On`
  - *Press the big white button the *CamFive* and the orange power switch on top of the K40.*


### Lightburn

- [ ] Lightburn is running on the desktop PC.
- [ ] Lightburn is connected to the laser cutter.
- [ ] Lightburn has the proper files to make the desired cuts and engravings.

### CamFive 2416

Work in Progress 😸

<!-- TODO: Some stuff about size, passthrough, rfid -->

### K40

Work in Progress 🚧

<!-- TODO: Some stuff about the size, controls, rotary, z-table, focus adjust -->

### Claymore

👷 🚧 *Under Construction* 🚧 👷

## Cutting and Engraving

### Speed and Power

There are two ways to attenuate the laser when etching and cutting.

- Speed of the cutting head, generally measured in mm/sec
- Power of the laser, generally measured as a percentage

**Note:** is best to only change one of these variables at a time when dialing in your cuts. Cuts are going to usually have a low speed and high power (ex. speed : 12mm/sec, power : 80% for 1/4" plywood), while engravings can be done at a high speed with a low power (ex. 400mm/sec, 35% for a shallow engraving on wood or acrylic regardless of thickness).

For example, if you try to cut through the material and is isn't going through, then bump the speed down and try again. In the case that you haven't moved your material yet, and it didn't cut as you intended, you can keep making passes with the laser.

### What we can cut

#### Wood

### What we can engrave

### What we cannot cut

## Troubleshooting

### CamFive

Q: The laser unexpectedly shut off when cutting. What should I do to not ruin my project?

A: **Don't move your project!** Wait for the machine to reset, and press `enter` when prompted. If the controller on the machine is off/unlit, then turn off the machine and turn it back on. When prompted press `enter`.

Q: The laser is beeping at me everytime I try to cut and says `frame slop`

A: Your cut is out of bounds or your origin is improperly set. Check to make sure you don't have any objects set to print outside of the cutting bed in Lightburn. Then double check your `origin`. If you're using the camera to align your cuts, then make sure your orgin is set to `absolute coordinates`.

### FAQ

Q: How do I determine what to use for power and speed settings?

A: You may refer to the sheet that describes successful cut settings. This is *hopefully* located somewhere near the machine. Though there are a lot of materials not listed, you can use the sheet as a reference when cutting materials with different

## Resources

Josh and Derek

ATX Hackerspace

Boxes.py

G Drive